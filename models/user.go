package models

import (
	"time"
)

type Login struct {
	Email    string
	Password string
}

type User struct {
	ID        int     `json:"id"`
	Fullname  string  `json:"fullname"`
	Email     string  `json:"email"`
	Password  *string `json:"password"`
	Address   *string `json:"address"`
	Telephone *string `json:"telephone"`
}

type Auth struct {
	UserID      int       `json:"user_id"`
	AccessToken string    `json:"access_token"`
	ExpiredAt   time.Time `json:"expired_at"`
}

type UserInfo struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Profile       string `json:profile`
	Picture       string `json:"picture"`
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	Gender        string `json:"gender"`
}

type ResetPassword struct {
	Email    string
	Password string
}
