package models

type ErrorResponse struct {
	Error struct {
		Message string `json:"message"`
		Code    string `json:"code"`
	} `json:"error"`
}

func InvalidRequestResponse() ErrorResponse {
	res := ErrorResponse{}
	res.Error.Message = "Invalid request."
	res.Error.Code = "400"
	return res
}

func UnauthorizedResponse() ErrorResponse {
	res := ErrorResponse{}
	res.Error.Message = "Unauthorized."
	res.Error.Code = "401"
	return res
}

func ExistsResponse() ErrorResponse {
	res := ErrorResponse{}
	res.Error.Message = "Email already exists."
	res.Error.Code = "409"
	return res
}

func InternalServerErrorResponse() ErrorResponse {
	res := ErrorResponse{}
	res.Error.Message = "Internal server error. Backend problem. :("
	res.Error.Code = "500"
	return res
}

type SuccessResponse struct {
	Success struct {
		Message string `json:"message"`
		Code    string `json:"code"`
	} `json:"success"`
}

func OkResponse() SuccessResponse {
	res := SuccessResponse{}
	res.Success.Message = "Ok. :)"
	res.Success.Code = "200"
	return res
}
