package controllers

import (
	"Paiman_Bandi/db"
	"Paiman_Bandi/models"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"html/template"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

func GenToken() string {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()

	signedToken, _ := token.SignedString([]byte("paiman"))
	return signedToken
}

func Login(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method:", r.Method)
	switch r.Method {
	case "GET":
		t, _ := template.ParseFiles("views/login.html")
		t.Execute(w, nil)
	case "POST":

		var login models.Login
		if r.Body == nil {
			log.Fatalln("Send a request body!")
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(models.InvalidRequestResponse())
		}
		err := json.NewDecoder(r.Body).Decode(&login)
		if err != nil {
			log.Fatalln("Can't decode the request body!")
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(models.InvalidRequestResponse())
		}

		email := login.Email
		password := login.Password

		goauth, err := db.DBCon.Query("SELECT id FROM user WHERE email=? AND oauth='google'", email)
		if err != nil {
			log.Fatalln("Error querying.")
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		var gid int
		for goauth.Next() {
			err := goauth.Scan(&gid)
			if err != nil {
				log.Fatalln("Error scanning row.")
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}
		}

		if gid > 0 {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(models.InvalidRequestResponse())
		}

		rows, err := db.DBCon.Query("SELECT id FROM user WHERE email=? AND password=?", email, password)
		if err != nil {
			log.Fatalln("Error querying.")
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		var id int
		for rows.Next() {
			err := rows.Scan(&id)
			if err != nil {
				log.Fatalln("Error scanning row.")
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}
		}

		if id > 0 {
			signedToken := GenToken()
			expireToken := time.Now().Add(time.Hour * 24)
			StoreAuth(w, r, int(id), signedToken, expireToken)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(models.UnauthorizedResponse())
		}
	}
}

func Oauth2Callback(w http.ResponseWriter, r *http.Request) {

	code := r.URL.Query().Get("code")

	conf := &oauth2.Config{
		ClientID:     "489676815936-0ekicnodecbvqmqaqccht5h0t20cpug8.apps.googleusercontent.com",
		ClientSecret: "lHJqArU1RrKg2wo4vBQDCnUe",
		RedirectURL:  "http://pgolang.tk/oauth2callback",
		Endpoint:     google.Endpoint,
	}

	token, err := conf.Exchange(oauth2.NoContext, code)
	if err != nil {
		log.Println("Can't get token from Google Oauth2.", err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
	}

	client := conf.Client(oauth2.NoContext, token)
	res, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		log.Println("Can't get data from Google Oauth2.", err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
	}

	defer res.Body.Close()
	contents, err := ioutil.ReadAll(res.Body)

	var ui models.UserInfo
	err = json.Unmarshal(contents, &ui)
	if err != nil {
		log.Println("Can't parse response.", err)
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
	}

	signedToken := GenToken()
	expireToken := time.Now().Add(time.Hour * 24)

	log.Println(ui.Email)

	rows, err := db.DBCon.Query("SELECT id FROM user WHERE email=?", ui.Email)
	if err != nil {
		log.Fatalln("Error querying.")
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
	}

	var id int
	for rows.Next() {
		err := rows.Scan(&id)
		if err != nil {
			log.Fatalln("Error scanning row.")
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}
	}

	if id > 0 {
		StoreAuth(w, r, id, signedToken, expireToken)
		http.Redirect(w, r, "/dashboard", 301)
	} else {

		stmt, err := db.DBCon.Prepare("INSERT user SET fullname=?,email=?,oauth=?")
		if err != nil {
			log.Fatalln("Error preparing query.", err)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		res, err := stmt.Exec(ui.Name, ui.Email, "google")
		if err != nil {
			log.Fatalln("Error executing query.", err)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		newId, err := res.LastInsertId()
		if err != nil {
			log.Fatalln("Error getting last insert id")
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		StoreAuth(w, r, int(newId), signedToken, expireToken)
		http.Redirect(w, r, "/dashboard", 301)
	}
}

func StoreAuth(w http.ResponseWriter, r *http.Request, userId int, signedToken string, expireToken time.Time) {
	//-- check auth
	rows, err := db.DBCon.Query("SELECT id FROM auth WHERE user_id=?", userId)
	if err != nil {
		log.Fatalln("Error querying.")
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
	}

	var id int
	for rows.Next() {
		err := rows.Scan(&id)
		if err != nil {
			log.Fatalln("Error scanning row.")
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}
	}

	if id > 0 {
		//-- update auth
		stmt, err := db.DBCon.Prepare("UPDATE auth SET access_token=?, expired_at=? WHERE id=?")
		if err != nil {
			log.Fatalln("Error preparing query.", err)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		_, err = stmt.Exec(signedToken, expireToken, id)
		if err != nil {
			log.Fatalln("Error executing query.", err)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		http.SetCookie(w, &http.Cookie{
			Name:   "token",
			Value:  "Bearer " + signedToken,
			MaxAge: 86400,
		})

		http.SetCookie(w, &http.Cookie{
			Name:   "id",
			Value:  strconv.Itoa(id),
			MaxAge: 86400,
		})
	} else {
		//-- insert auth
		stmt, err := db.DBCon.Prepare("INSERT auth SET user_id=?,access_token=?,expired_at=?")
		if err != nil {
			log.Fatalln("Error preparing query.", err)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		res, err := stmt.Exec(userId, signedToken, expireToken)
		if err != nil {
			log.Fatalln("Error executing query.", err)
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		newId, err := res.LastInsertId()
		if err != nil {
			log.Fatalln("Error getting last insert id")
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		http.SetCookie(w, &http.Cookie{
			Name:   "token",
			Value:  "Bearer " + signedToken,
			MaxAge: 86400,
		})

		http.SetCookie(w, &http.Cookie{
			Name:   "id",
			Value:  strconv.Itoa(int(newId)),
			MaxAge: 86400,
		})
	}

	goauth, err := db.DBCon.Query("SELECT oauth FROM user WHERE id=?", userId)
	if err != nil {
		log.Fatalln("Error querying.")
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
	}

	var oauth *string
	for goauth.Next() {
		err := goauth.Scan(&oauth)
		if err != nil {
			log.Fatalln("Error scanning row.")
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}
	}

	if oauth == nil {
		auth := models.Auth{
			UserID:      userId,
			AccessToken: signedToken,
			ExpiredAt:   expireToken,
		}
		json.NewEncoder(w).Encode(auth)
	}
}

func randString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	if ValidateAuth(w, r) {
		fmt.Println("method:", r.Method)
		switch r.Method {
		case "POST":

			cookieB, _ := r.Cookie("id")
			log.Println(cookieB.Value)

			id := cookieB.Value

			stmt, err := db.DBCon.Prepare("UPDATE auth SET access_token=?,expired_at=? WHERE id=?")
			if err != nil {
				log.Fatalln("Error preparing query.", err)
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}

			_, err = stmt.Exec(nil, nil, id)
			if err != nil {
				log.Fatalln("Error executing query.", err)
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}

			http.SetCookie(w, &http.Cookie{
				Name:   "token",
				Value:  "",
				MaxAge: 0,
			})

			http.SetCookie(w, &http.Cookie{
				Name:   "id",
				Value:  "",
				MaxAge: 0,
			})
		}
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(models.UnauthorizedResponse())
	}
}
