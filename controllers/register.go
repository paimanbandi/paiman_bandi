package controllers

import (
	"Paiman_Bandi/db"
	"Paiman_Bandi/models"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func Register(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method:", r.Method)

	switch r.Method {
	case "GET":
		t, _ := template.ParseFiles("views/register.html")
		t.Execute(w, nil)
	case "POST":
		var u models.User
		if r.Body == nil {
			http.Error(w, "Please send a request body", 400)
			return
		}
		err := json.NewDecoder(r.Body).Decode(&u)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		rows, err := db.DBCon.Query("SELECT count(*) FROM user WHERE email=?", u.Email)
		if err != nil {
			log.Fatalln("Error querying.")
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
		}

		var count int
		for rows.Next() {
			err := rows.Scan(&count)
			if err != nil {
				log.Fatalln("Error scanning row.")
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}
		}

		if count > 0 {
			w.WriteHeader(http.StatusConflict)
			json.NewEncoder(w).Encode(models.ExistsResponse())
		} else {
			stmt, err := db.DBCon.Prepare("INSERT user SET fullname=?,password=?,email=?,address=?,telephone=?")
			if err != nil {
				log.Fatalln("Error preparing query.", err)
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}

			res, err := stmt.Exec(u.Fullname, u.Password, u.Email, u.Address, u.Telephone)
			if err != nil {
				log.Fatalln("Error executing query.", err)
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}

			id, err := res.LastInsertId()
			if err != nil {
				log.Fatalln("Error getting last insert id")
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}

			fmt.Println(id)

			if id > 0 {
				json.NewEncoder(w).Encode(models.OkResponse())
			} else {
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}
		}

	}

}
