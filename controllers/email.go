package controllers

import (
	"Paiman_Bandi/db"
	"Paiman_Bandi/models"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/smtp"
	"strconv"
	"strings"
	"time"
)

type Mail struct {
	senderId string
	toIds    []string
	toId     string
	subject  string
	body     string
}

type SmtpServer struct {
	host string
	port string
}

func (s *SmtpServer) ServerName() string {
	return s.host + ":" + s.port
}

func (mail *Mail) BuildMessage() string {
	message := ""
	message += fmt.Sprintf("From: %s\r\n", mail.senderId)
	if len(mail.toIds) > 0 {
		message += fmt.Sprintf("To: %s\r\n", strings.Join(mail.toIds, ";"))
	}

	message += fmt.Sprintf("Subject: %s\r\n", mail.subject)
	message += "\r\n" + mail.body

	return message
}

func SendLink(res http.ResponseWriter, req *http.Request) {

	var resetPwd models.ResetPassword
	if req.Body == nil {
		log.Fatalln("Send a request body!")
		res.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(res).Encode(models.InvalidRequestResponse())
	}
	err := json.NewDecoder(req.Body).Decode(&resetPwd)
	if err != nil {
		log.Fatalln("Can't decode the request body.")
		res.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(res).Encode(models.InvalidRequestResponse())
	}

	email := resetPwd.Email

	signedToken := GenToken()
	expireToken := time.Now().Add(time.Hour * 24)

	rows, err := db.DBCon.Query("SELECT id FROM user WHERE email=?", email)
	if err != nil {
		log.Fatalln("Error querying.")
		res.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(res).Encode(models.InternalServerErrorResponse())
	}

	var id int
	for rows.Next() {
		err := rows.Scan(&id)
		if err != nil {
			log.Fatalln("Error scanning row.")
			res.WriteHeader(http.StatusConflict)
			json.NewEncoder(res).Encode(models.InternalServerErrorResponse())
		}
	}

	var link string

	if id > 0 {
		//-- uri parameter is just random string, we play StoreAuth
		StoreAuth(res, req, int(id), signedToken, expireToken)
		link = req.Host + "/reset-password?rand=" + randString(8) + strconv.Itoa(id) + signedToken + randString(4)

		mail := Mail{}
		mail.senderId = "pgolang.inc@gmail.com"
		mail.toIds = []string{email}
		mail.subject = "Reset Password"
		mail.body = "Please click the link below to reset your password.\n\n" + link

		messageBody := mail.BuildMessage()

		smtpServer := SmtpServer{host: "smtp.gmail.com", port: "465"}

		log.Println(smtpServer.host)

		auth := smtp.PlainAuth("", mail.senderId, "visitindonesia", smtpServer.host)

		tlsconfig := &tls.Config{
			InsecureSkipVerify: true,
			ServerName:         smtpServer.host,
		}

		conn, err := tls.Dial("tcp", smtpServer.ServerName(), tlsconfig)
		if err != nil {
			log.Panic(err)
		}

		client, err := smtp.NewClient(conn, smtpServer.host)
		if err != nil {
			log.Panic(err)
		}

		if err = client.Auth(auth); err != nil {
			log.Panic(err)
		}

		if err = client.Mail(mail.senderId); err != nil {
			log.Panic(err)
		}
		for _, k := range mail.toIds {
			if err = client.Rcpt(k); err != nil {
				log.Panic(err)
			}
		}

		w, err := client.Data()
		if err != nil {
			log.Panic(err)
		}

		_, err = w.Write([]byte(messageBody))
		if err != nil {
			log.Panic(err)
		}

		err = w.Close()
		if err != nil {
			log.Panic(err)
		}

		client.Quit()

		log.Println("E-mail sent successfully")
	} else {
		res.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(res).Encode(models.InvalidRequestResponse())
	}
}

func ResetPassword(res http.ResponseWriter, req *http.Request) {

	if ValidateAuth(res, req) {
		fmt.Println("method:", req.Method)
		switch req.Method {
		case "GET":
			t, _ := template.ParseFiles("views/reset-password.html")
			t.Execute(res, nil)
		case "POST":
			var resetPwd models.ResetPassword
			if req.Body == nil {
				log.Fatalln("Send a request body!")
				res.WriteHeader(http.StatusBadRequest)
				json.NewEncoder(res).Encode(models.InvalidRequestResponse())
			}
			err := json.NewDecoder(req.Body).Decode(&resetPwd)
			if err != nil {
				log.Fatalln("Can't decode the request body.")
				res.WriteHeader(http.StatusBadRequest)
				json.NewEncoder(res).Encode(models.InvalidRequestResponse())
			}

			cookie, _ := req.Cookie("id")
			id := cookie.Value
			pwd := resetPwd.Password
			log.Println("id = " + cookie.Value)
			log.Println("pwd =", pwd)

			stmt, err := db.DBCon.Prepare("UPDATE user SET password=? WHERE id=?")
			if err != nil {
				log.Fatalln("Error preparing query.", err)
				res.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(res).Encode(models.InternalServerErrorResponse())
			}

			_, err = stmt.Exec(pwd, id)
			if err != nil {
				log.Fatalln("Error executing query.", err)
				res.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(res).Encode(models.InternalServerErrorResponse())
			}
		}
	} else {
		res.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(res).Encode(models.UnauthorizedResponse())
	}

}
