package controllers

import (
	"Paiman_Bandi/db"
	"Paiman_Bandi/models"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"html/template"
	"log"
	"net/http"
	"strings"
)

func ValidateAuth(w http.ResponseWriter, r *http.Request) bool {
	//format: Authorization: Bearer <token>

	auth := r.Header.Get("Authorization")
	userId := r.Header.Get("user_id")

	if auth == "" || userId == "" {
		cookieA, _ := r.Cookie("token")
		log.Println(cookieA.Value)

		cookieB, _ := r.Cookie("id")
		log.Println(cookieB.Value)

		auth = cookieA.Value
		userId = cookieB.Value
	}

	if strings.HasPrefix(auth, "Bearer") {
		auth = strings.TrimPrefix(auth, "Bearer ")
		log.Println("auth", auth)

		token, err := jwt.Parse(auth, func(token *jwt.Token) (interface{}, error) {
			return []byte("paiman"), nil
		})

		log.Println("[INFO >> Token Info]", token)

		if err == nil && token.Valid {
			rows, err := db.DBCon.Query("SELECT count(*) FROM auth WHERE user_id=? AND access_token=?", userId, auth)
			if err != nil {
				log.Fatalln("Error querying.")
			}

			var count int
			for rows.Next() {
				err := rows.Scan(&count)
				if err != nil {
					log.Fatalln("Error scanning row.")
				}
			}

			if count == 1 {
				return true
			}
			return false
		}
		return false
	} else {
		return false
	}
	return false
}

func GetProfile(w http.ResponseWriter, r *http.Request) {

	if ValidateAuth(w, r) {
		userId := r.Header.Get("user_id")
		fmt.Println("method:", r.Method)

		switch r.Method {
		case "GET":
			rows, err := db.DBCon.Query("SELECT id, fullname, email, password, address, telephone FROM user WHERE id=?", userId)
			if err != nil {
				log.Fatalln("Error querying.")
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}

			var id int
			var fullname string
			var email string
			var password *string
			var address *string
			var telephone *string

			for rows.Next() {
				err := rows.Scan(&id, &fullname, &email, &password, &address, &telephone)
				if err != nil {
					log.Fatalln("Error scanning row.", err)
					w.WriteHeader(http.StatusInternalServerError)
					json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
				}
			}

			user := models.User{
				ID:        id,
				Fullname:  fullname,
				Email:     email,
				Password:  password,
				Address:   address,
				Telephone: telephone,
			}

			json.NewEncoder(w).Encode(user)
		case "POST":

			var u models.User
			if r.Body == nil {
				http.Error(w, "Please send a request body", 400)
				return
			}
			err := json.NewDecoder(r.Body).Decode(&u)
			if err != nil {
				http.Error(w, err.Error(), 400)
				return
			}

			stmt, err := db.DBCon.Prepare("UPDATE user SET fullname=?,password=?,email=?, address=?, telephone=? WHERE id=?")
			if err != nil {
				log.Fatalln("Error preparing query.", err)
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}

			_, err = stmt.Exec(u.Fullname, u.Password, u.Email, u.Address, u.Telephone, userId)
			if err != nil {
				log.Fatalln("Error executing query.", err)
				w.WriteHeader(http.StatusInternalServerError)
				json.NewEncoder(w).Encode(models.InternalServerErrorResponse())
			}

			json.NewEncoder(w).Encode(models.OkResponse())
		}

	} else {
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(models.UnauthorizedResponse())
	}
}

func Dashboard(w http.ResponseWriter, r *http.Request) {
	if ValidateAuth(w, r) {
		fmt.Println("method:", r.Method)
		switch r.Method {
		case "GET":
			t, _ := template.ParseFiles("views/profile.html")
			t.Execute(w, nil)
		}
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(models.UnauthorizedResponse())
	}
}
