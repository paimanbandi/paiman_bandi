package main

import (
	"Paiman_Bandi/controllers"
	"Paiman_Bandi/db"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"net/http"
)

func main() {

	var err error
	db.DBCon, err = sql.Open("mysql",
		"paiman:pgolang@tcp(45.77.39.172:3306)/pgolang?parseTime=true")
	if err != nil {
		log.Println("Can't connect database.")
	} else {
		log.Println("Connecting to the database.")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Paiman Golang Test")
	})

	http.HandleFunc("/register", func(w http.ResponseWriter, r *http.Request) {
		controllers.Register(w, r)
	})

	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		controllers.Login(w, r)
	})

	http.HandleFunc("/login-google", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&redirect_uri=http%3A%2F%2Fpgolang.tk%2Foauth2callback&access_type=offline&response_type=code&client_id=489676815936-0ekicnodecbvqmqaqccht5h0t20cpug8.apps.googleusercontent.com", 301)
	})

	http.HandleFunc("/profile", func(w http.ResponseWriter, r *http.Request) {
		controllers.GetProfile(w, r)
	})

	http.HandleFunc("/oauth2callback", func(w http.ResponseWriter, r *http.Request) {
		controllers.Oauth2Callback(w, r)
	})

	http.HandleFunc("/dashboard", func(w http.ResponseWriter, r *http.Request) {
		controllers.Dashboard(w, r)
	})

	http.HandleFunc("/logout", func(w http.ResponseWriter, r *http.Request) {
		controllers.Logout(w, r)
	})

	http.HandleFunc("/send-link", func(w http.ResponseWriter, r *http.Request) {
		controllers.SendLink(w, r)
	})

	http.HandleFunc("/reset-password", func(w http.ResponseWriter, r *http.Request) {
		controllers.ResetPassword(w, r)
	})

	log.Fatal(http.ListenAndServe(":80", nil))

}
