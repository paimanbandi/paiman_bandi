CREATE TABLE user (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    fullname VARCHAR(50) NOT NULL, 
    email VARCHAR(50) NOT NULL,
    password VARCHAR(255) NULL,
    address VARCHAR(255) NULL,
    telephone VARCHAR(15) NULL,
    oauth VARCHAR(15) NULL
);

CREATE TABLE auth (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT NOT NULL,
    access_token VARCHAR(255) NULL,
    expired_at DATETIME NULL
);